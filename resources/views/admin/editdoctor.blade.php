<!DOCTYPE html>
<html lang="en">
  <head>
      <base href="/public">
      <style type="text/css">
          label
          {
            display: inline-block;
            width: 200px;
          }
      </style>
    @include('admin.css')
  </head>
  <body>
    <div class="container-scroller">
      <!-- partial:partials/_sidebar.html -->
      @include('admin.sidebar')
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_navbar.html -->
        @include('admin.navbar')
        <!-- partial -->
        <div class="container-fluid page-body-wrapper">
            <div class="container" align="center" style="padding: 100px">
                <form action="{{ url('updatedoctor',$data->id) }}" method="post" enctype="multipart/form-data"> 
                    @csrf
                    <div style="padding: 15px;">
                        <label>Doctor Name:</label>
                        <input type="text" style="color:black" name="name" value="{{ $data->name }}">
                    </div>
                    <div style="padding: 15px;">
                        <label>Doctor phone:</label>
                        <input type="number" style="color:black" name="phone" value="{{ $data->phone }}">
                    </div>
                    <div style="padding: 15px;">
                        <label>Speciality</label>
                        <input type="text" style="color: black" name="speciality" value="{{ $data->speciality }}">
                    </div>
                    <div style="padding: 15px;">
                        <label>Doctor Room:</label>
                        <input type="text" style="color:black" name="room" value="{{ $data->room }}">
                    </div>
                    <div style="padding: 15px;">
                        <img height="150px" width="150px" src="doctorimage/{{ $data->image }}">
                    </div>
                    <div style="padding: 15px;">
                        <label>Change Image:</label>
                        <input type="file" name="file">
                    </div>
                    <div style="padding: 15px;">
                        <input type="submit" class="btn btn-success">
                    </div>
                </form>
            </div>
        </div>
        <!-- main-panel ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    @include('admin.script')
  </body>
</html>