<!DOCTYPE html>
<html lang="en">
  <head>
    @include('admin.css')
  </head>
  <body>
    <div class="container-scroller">
      <!-- partial:partials/_sidebar.html -->
      @include('admin.sidebar')
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_navbar.html -->
        @include('admin.navbar')
        <!-- partial -->
        <div class="container-fluid page-body-wrapper">
            <div style="padding-top:100px; margin-left:-121px;">
                <table>
                    <tr style="background-color: green">
                        <th style="padding:10px; color:white;">Customer Name</th>
                        <th style="padding:10px; color:white;">Email</th>
                        <th style="padding:10px; color:white;">Phone</th>
                        <th style="padding:10px; color:white;">Doctor</th>
                        <th style="padding:10px; color:white;">Date</th>
                        <th style="padding:10px; color:white;">Message</th>
                        <th style="padding:10px; color:white;">Status</th>
                        <th style="padding:10px; color:white;">Approved</th>
                        <th style="padding:10px; color:white;">Cancle</th>
                        <th style="padding:10px; color:white;">Send Email</th>

                    </tr>

                    @foreach ($data as $appoint)   
                        <tr style="background-color: rgb(255, 90, 14);">
                            <td style="padding:10px; color:white;">{{ $appoint->name }}</td>
                            <td style="padding:10px; color:white;">{{ $appoint->email }}</td>
                            <td style="padding:10px; color:white;">{{ $appoint->phone }}</td>
                            <td style="padding:10px; color:white;">{{ $appoint->doctor }}</td>
                            <td style="padding:10px; color:white;">{{ $appoint->date }}</td>
                            <td style="padding:10px; color:white;">{{ $appoint->message }}</td>
                            <td style="padding:10px; color:white;">{{ $appoint->status }}</td>
                            <td>
                                <a class="btn btn-success" href="{{ url('approved',$appoint->id) }}">Approved</a>
                            </td>

                            <td>
                                <a class="btn btn-danger" href="{{ url('cancle',$appoint->id) }}">Cancle</a>
                            </td>
                            <td>
                              <a class="btn btn-primary" href="{{ url('emailview',$appoint->id) }}">Send Email</a>
                          </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
        <!-- main-panel ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    @include('admin.script')
  </body>
</html>