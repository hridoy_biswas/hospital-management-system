<!DOCTYPE html>
<html lang="en">
  <head>
      <style>
          label
          {
              display: inline-block;
              width: 200px;
          }
      </style>
    @include('admin.css')
  </head>
  <body>
    <div class="container-scroller">
      <!-- partial:partials/_sidebar.html -->
      @include('admin.sidebar')
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_navbar.html -->
        @include('admin.navbar')
        <div class="container-fluid page-body-wrapper">
           <div class="container" align="center" style="padding-top: 100px;">

                @if(session()->has('message'))
                    <div class="alert alert-success" role="alert">
                        <button type="button" class="btn-close" aria-label="close"></button>
                        {{ session()->get('message') }}
                    </div>
                @endif

                <form action="{{ url('upload_doctor') }}" method="post" enctype="multipart/form-data"> 
                    @csrf
                    <div style="padding: 15px;">
                        <label>Doctor Name:</label>
                        <input type="text" style="color:black" name="name" placeholder="Write Doctor Name" required>
                    </div>
                    <div style="padding: 15px;">
                        <label>Doctor phone:</label>
                        <input type="number" style="color:black" name="phone" placeholder="Write Doctor phone" required>
                    </div>
                    <div style="padding: 15px;">
                        <label>Speciality</label>
                        <select name="speciality" style="color:black; width:210px;" required>
                            <option>---Select---</option>
                            <option value="skin">Skin</option>
                            <option value="heart">Heart</option>
                            <option value="eye">Eye</option>
                            <option value="nose">Nose</option>
                        </select>
                    </div>
                    <div style="padding: 15px;">
                        <label>Doctor Room:</label>
                        <input type="text" style="color:black" name="room" placeholder="Write Doctor room" required>
                    </div>
                    <div style="padding: 15px;">
                        <label>Doctor Image:</label>
                        <input type="file" name="file" required>
                    </div>
                    <div style="padding: 15px;">
                        <input type="submit" class="btn btn-success">
                    </div>
                </form>
            </div>

            
        </div>

      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    @include('admin.script')
  </body>
</html>