<!DOCTYPE html>
<html lang="en">
  <head>
    @include('admin.css')
  </head>
  <body>
    <div class="container-scroller">
      <!-- partial:partials/_sidebar.html -->
      @include('admin.sidebar')
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_navbar.html -->
        @include('admin.navbar')
        <!-- partial -->
        <div class="container-fluid page-body-wrapper">
            <div align="center" style="padding-top:100px;">
                @if(session()->has('message'))
                    <div class="alert alert-success" role="alert">
                        <button type="button" class="btn-close" aria-label="close"></button>
                        {{ session()->get('message') }}
                    </div>
                @endif

                <table class="table table-bordered" style="margin-left: -125px;">
                    <tr style="background-color: green">
                        <th style="padding:10px; color:white;">Sl</th>
                        <th style="padding:10px; color:white;">Doctor Name</th>
                        <th style="padding:10px; color:white;">Phone</th>
                        <th style="padding:10px; color:white;">Speciality</th>
                        <th style="padding:10px; color:white;">Room</th>
                        <th style="padding:10px; color:white;">Image</th>
                        <th style="padding:10px; color:white;">Edit</th>
                        <th style="padding:10px; color:white;">Delete</th>
                    </tr>
                    <?php $sl = 1; ?>
                    @foreach ($data as $doctor)   
                        <tr style="background-color: rgb(255 70 0);">
                            <td style="padding:10px; color:white;">{{ $sl++ }}</td>
                            <td style="padding:10px; color:white;">{{ $doctor->name }}</td>
                            <td style="padding:10px; color:white;">{{ $doctor->phone }}</td>
                            <td style="padding:10px; color:white;">{{ $doctor->speciality }}</td>
                            <td style="padding:10px; color:white;">{{ $doctor->room }}</td>
                            <td style="padding:10px; color:white;"><img style="height: 100px; width:215px" src="doctorimage/{{ $doctor->image }}"></td>
                            <td><a  class="btn btn-success" href="{{ url('editdoctor',$doctor->id) }}">Edit Doctor</a></td>
                            <td><a onclick="return confirm('Are You Sure You Want to Delete Doctor')" class="btn btn-danger" href="{{ url('deletedoctor',$doctor->id) }}">Delete Doctor</a></td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>

        <!-- main-panel ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    @include('admin.script')
  </body>
</html>